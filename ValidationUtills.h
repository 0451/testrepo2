#pragma once
#include <string>
#include <time.h>
#include "ConvertUtills.h"
using namespace std;

class ValidationUtills
{

public:

	static bool isNumber(string str);
	static bool isPositiveNumber(string str);
	static bool notEmptyString(string str);
	static bool dateIsCorrect(tm* date);
	static bool dateIsCorrect(string str);

};

