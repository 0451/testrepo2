#include "RoomBase.h"

bool RoomBase::fingByIdAndUpdate(int id, Room* room)
{
	return false;
}

vector<Room> RoomBase::findAll()
{
	return vector<Room>();
}

vector<Room> RoomBase::findByRentedAndStreetAndMoneyAndSquare(bool rented, string address, int money, double square)
{
	return vector<Room>();
}

vector<Room> RoomBase::findByRentedSortedByAddress(bool rented)
{
	return vector<Room>();
}
