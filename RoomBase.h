#pragma once
#include "Room.h"
#include <vector>

class RoomBase
{


public:

	/**
	* Find room from base by ID and replace it 
	*
	* @param id ID of searched Room
	* @param room Room for replace
	*
	* @return Is room replaced successfuly
	**/
	bool fingByIdAndUpdate(int id, Room* room);

	/**
	* Find all rooms from base
	*
	* @return Vector<Room> with all romms from base
	**/
	vector<Room> findAll();

	/**
	* Find rooms by conditions
	*
	* @param rented Is searching room rented
	* @param adress Searching room adress
	* @param money Bottom border of room month cost
	* @param square Bottom border of room square
	*
	* @return Vector<Room> with rooms satisfying conditions
	**/
	vector<Room> findByRentedAndStreetAndMoneyAndSquare(bool rented, string adress, int money, double square);

	/**
	* Find all rooms by Romm.rented condition sorted by Room.adress
	*
	* @param rented Is searcing rooms rented
	*
	* @return Vector<Room> with Romm.rented == rented and sorted by adress
	**/
	vector<Room> findByRentedSortedByAddress(bool rented);

private:

	string baseFilename = "roomBase.dat";

};

