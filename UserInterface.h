#pragma once
#include "Room.h"
#include <vector>
#include "RoomBase.h"
#include "RentController.h"

class UserInterface
{

public:

	vector<Room> currentRoomList = vector<Room>();
	int selectedRoomIndex = -1;
	int rentDays = -1;
	tm* startDate;
	tm* endDate;

	RoomBase* roomBase;
	RentController* rentController;

public:

	UserInterface(RoomBase* roomBase, RentController* rentController)
	{
		this->roomBase = roomBase;
		this->rentController = rentController;
	}

	/**
	* Calc room cost for some days
	*
	* @return romm cost for rented days
	**/
	double getCurrentRoomCost();

	/**
	* Method which colling when "search" button clicked
	*
	* @param rented Is searching room rented
	* @param adress Searching room adress
	* @param money Bottom border of room month cost
	* @param square Bottom border of room square
	*
	* @return Is search request valid and executed
	**/
	bool searchRoomByStreetAndMoneyAndSqure(bool rented, string address, int square, int money);

	/**
	* Load rooms to current room list
	*
	* @return Is loading done successfuly
	**/
	bool getAllRooms();

	/**
	* Method which colling when "rent" button clicked
	*
	* @return Is room successfuly rented
	**/
	bool takeRoom(Renter* renter, int sumToPay);

	/**
	* Update rent end date varible with rent start date and rent days
	**/
	void updateRentEndDate();

};

