#include "MyForm.h"
#include <Windows.h>

using namespace EmployeeGUI;

int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew Form1);
}