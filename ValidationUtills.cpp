#include "ValidationUtills.h"
#include <regex>

bool ValidationUtills::isNumber(string str)
{
	regex numRegex("\\d+");
	return regex_match(str, numRegex);
}

bool ValidationUtills::isPositiveNumber(string str)
{
	if(isNumber(str))
	{
		int res = stoi(str);
		if(res > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}



bool ValidationUtills::notEmptyString(string str)
{
	return str.length() > 0;
}

bool ValidationUtills::dateIsCorrect(tm* date)
{
	if(date != NULL)
	{
		time_t timeNow = time(0);
		tm currentDate;
		localtime_s(&currentDate, &timeNow);
		return date->tm_mday > 0 && date->tm_mday < 32
			&& date->tm_mon >= 0 && date->tm_mon < 12
			&& date->tm_year >= currentDate.tm_year && date->tm_year < currentDate.tm_year + 3;
	}
	else
	{
		return false;
	}
}

bool ValidationUtills::dateIsCorrect(string str)
{
	regex dateRegex("\\d{2}.\\d{2}.\\d{4}");
	if(regex_match(str, dateRegex))
	{
		tm* date = ConvertUtills::stringToDate(str);
		return dateIsCorrect(date);
	}
	else
	{
		return false;
	}
}
