#include "ConvertUtills.h"

string ConvertUtills::textBoxStringToString(String^ stringToConvert)
{
	char chars [50] = { 0 };
	if(stringToConvert->Length < sizeof(chars))
		sprintf(chars, "%s", stringToConvert);
	string stlString(chars);
	return stlString;
}

String^ ConvertUtills::stringToTextBoxString(string str)
{
	return gcnew String(str.c_str());
}

String^ ConvertUtills::dateToTextBoxString(tm* date)
{
	String^ todayDate = "";
	todayDate += (date->tm_mday / 10 == 0) ? ("0" + date->tm_mday) : "" + date->tm_mday;
	todayDate += ".";
	todayDate += ((date->tm_mon + 1) / 10 == 0) ? ("0" + (date->tm_mon + 1)) : "" + (date->tm_mon + 1) + "";
	todayDate += ".";
	todayDate += date->tm_year + 1900;
	return todayDate;
}

tm* ConvertUtills::textBoxStringToDate(String^ stringToConvert)
{
	string date = textBoxStringToString(stringToConvert);

	return stringToDate(date);
}

tm* ConvertUtills::stringToDate(string stringToConvert)
{
	time_t timeNow = time(0);
	tm* convertedDate = new tm();
	localtime_s(convertedDate, &timeNow);
	int day = 0;
	int month = 0;
	int year = 0;

	sscanf_s(stringToConvert.c_str(), "%d.%d.%d", &day, &month, &year);

	convertedDate->tm_mday = day;
	convertedDate->tm_mon = month - 1;
	convertedDate->tm_year = year - 1900;

	return convertedDate;
}
