#include "Room.h"

int Room::curentId = 0;

void Room::startRent(Renter* renter, int rentedDays)
{
	this->isRented = true;
	this->rentedDays = rentedDays;
	this->renter = renter;

	time_t timeNow = time(0);
	localtime_s(&rentStartDate, &timeNow);
	localtime_s(&rentEndDate, &timeNow);
	rentEndDate.tm_mday += rentedDays;
	timeNow = mktime(&rentEndDate);
	localtime_s(&rentEndDate, &timeNow);
}

void Room::endRent()
{
	this->isRented = false;
	this->rentedDays = -1;
	this->renter = NULL;
}

void Room::serealize(ostream& ofStream)
{
	tm tmBuffer;

	int addressSize = (this->address.size());
	ofStream.write(reinterpret_cast<char*>(&addressSize), sizeof(int));
	ofStream.write(this->address.c_str(), addressSize);

	ofStream.write((char*) &this->rentStartDate, sizeof(tm));
	ofStream.write((char*) &this->rentEndDate, sizeof(tm));

	ofStream.write((char*) this, sizeof(Room));
	this->renter->serealize(ofStream);
}

Room* Room::deserealize(istream& ofStream)
{
	Room* room = new Room();
	tm tmBuffer;

	int addressSize;
	char* buf;
	ofStream.read(reinterpret_cast<char*>(&addressSize), sizeof(int));
	buf = new char [addressSize];
	ofStream.read(buf, addressSize);
	room->address = "";
	room->address.append(buf, addressSize);

	ofStream.read((char*) &tmBuffer, sizeof(tm));
	room->rentStartDate = tmBuffer;
	ofStream.read((char*) &tmBuffer, sizeof(tm));
	room->rentEndDate = tmBuffer;

	ofStream.read((char*) room, sizeof(Room));

	room->renter = Renter::deserealize(ofStream);
	return room;
}