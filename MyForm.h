#pragma once
#include <msclr\marshal_cppstd.h>
#include "UserInterface.h"
#include "ValidationUtills.h"
#include "ConvertUtills.h"
#include <regex>
#include <regex>

namespace EmployeeGUI
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;

	public ref class Form1: public System::Windows::Forms::Form
	{
	public:

		UserInterface* userInterface;
		Form1(void)
		{
			userInterface = new UserInterface(new RoomBase(), new RentController());
			InitializeComponent();

			userInterface->getAllRooms();

			updateRoomList();

			time_t timeNow = time(0);
			tm* time = new tm();
			localtime_s(time, &timeNow);
			this->startDateTextField->Text = ConvertUtills::dateToTextBoxString(time);
			delete(time);
		}

	protected:
		~Form1()
		{
			delete(userInterface);
			if(components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ColumnHeader^ id;
	protected:
	private: System::Windows::Forms::ColumnHeader^ address;
	private: System::Windows::Forms::ColumnHeader^ sqare;
	private: System::Windows::Forms::ColumnHeader^ cost;

	private: System::Windows::Forms::GroupBox^ rommListBox;

	private: System::Windows::Forms::GroupBox^ rentProcessBox;


	private: System::Windows::Forms::Label^ endRentDateLabel;


	private: System::Windows::Forms::Label^ rentDaysLabel;


	private: System::Windows::Forms::TextBox^ rentDaysTextField;
	private: System::Windows::Forms::Label^ startRentDateLabel;


	private: System::Windows::Forms::TextBox^ startDateTextField;
	private: System::Windows::Forms::Label^ fathLabel;


	private: System::Windows::Forms::TextBox^ fathTextField;
	private: System::Windows::Forms::Label^ lastNameLabel;


	private: System::Windows::Forms::TextBox^ lastNameTextField;
	private: System::Windows::Forms::Label^ firstNameLabel;


	private: System::Windows::Forms::TextBox^ firstNameTextField;
	private: System::Windows::Forms::Button^ rentBtn;
	private: System::Windows::Forms::ListView^ roomList;
	private: System::Windows::Forms::RichTextBox^ finalCostTextField;
	private: System::Windows::Forms::Label^ finalCostLabel;
	private: System::Windows::Forms::RichTextBox^ endDateTextField;
	private: System::Windows::Forms::GroupBox^ searchBox;
	private: System::Windows::Forms::Label^ minCostLabel;
	private: System::Windows::Forms::Button^ searchBtn;
	private: System::Windows::Forms::TextBox^ minCostTextField;
	private: System::Windows::Forms::Label^ minSquareLabel;
	private: System::Windows::Forms::TextBox^ minSqureTextField;
	private: System::Windows::Forms::Label^ adressLabel;
	private: System::Windows::Forms::TextBox^ adressTextField;





	private:
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->roomList = (gcnew System::Windows::Forms::ListView());
			this->id = (gcnew System::Windows::Forms::ColumnHeader());
			this->address = (gcnew System::Windows::Forms::ColumnHeader());
			this->sqare = (gcnew System::Windows::Forms::ColumnHeader());
			this->cost = (gcnew System::Windows::Forms::ColumnHeader());
			this->rommListBox = (gcnew System::Windows::Forms::GroupBox());
			this->rentProcessBox = (gcnew System::Windows::Forms::GroupBox());
			this->endDateTextField = (gcnew System::Windows::Forms::RichTextBox());
			this->finalCostTextField = (gcnew System::Windows::Forms::RichTextBox());
			this->finalCostLabel = (gcnew System::Windows::Forms::Label());
			this->rentBtn = (gcnew System::Windows::Forms::Button());
			this->endRentDateLabel = (gcnew System::Windows::Forms::Label());
			this->rentDaysLabel = (gcnew System::Windows::Forms::Label());
			this->rentDaysTextField = (gcnew System::Windows::Forms::TextBox());
			this->startRentDateLabel = (gcnew System::Windows::Forms::Label());
			this->startDateTextField = (gcnew System::Windows::Forms::TextBox());
			this->fathLabel = (gcnew System::Windows::Forms::Label());
			this->fathTextField = (gcnew System::Windows::Forms::TextBox());
			this->lastNameLabel = (gcnew System::Windows::Forms::Label());
			this->lastNameTextField = (gcnew System::Windows::Forms::TextBox());
			this->firstNameLabel = (gcnew System::Windows::Forms::Label());
			this->firstNameTextField = (gcnew System::Windows::Forms::TextBox());
			this->searchBox = (gcnew System::Windows::Forms::GroupBox());
			this->adressTextField = (gcnew System::Windows::Forms::TextBox());
			this->adressLabel = (gcnew System::Windows::Forms::Label());
			this->minSqureTextField = (gcnew System::Windows::Forms::TextBox());
			this->minSquareLabel = (gcnew System::Windows::Forms::Label());
			this->minCostTextField = (gcnew System::Windows::Forms::TextBox());
			this->searchBtn = (gcnew System::Windows::Forms::Button());
			this->minCostLabel = (gcnew System::Windows::Forms::Label());
			this->rommListBox->SuspendLayout();
			this->rentProcessBox->SuspendLayout();
			this->searchBox->SuspendLayout();
			this->SuspendLayout();
			// 
			// roomList
			// 
			this->roomList->Activation = System::Windows::Forms::ItemActivation::OneClick;
			this->roomList->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(4)
			{
				this->id, this->address,
					this->sqare, this->cost
			});
			this->roomList->FullRowSelect = true;
			this->roomList->GridLines = true;
			this->roomList->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->roomList->HideSelection = false;
			this->roomList->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->roomList->Location = System::Drawing::Point(15, 37);
			this->roomList->MultiSelect = false;
			this->roomList->Name = L"roomList";
			this->roomList->Size = System::Drawing::Size(478, 523);
			this->roomList->TabIndex = 0;
			this->roomList->UseCompatibleStateImageBehavior = false;
			this->roomList->View = System::Windows::Forms::View::Details;
			this->roomList->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::roomList_SelectedIndexChanged);
			// 
			// id
			// 
			this->id->Text = L"id";
			this->id->Width = 25;
			// 
			// address
			// 
			this->address->Text = L"address";
			this->address->Width = 135;
			// 
			// sqare
			// 
			this->sqare->Text = L"sqare (m^3)";
			this->sqare->Width = 68;
			// 
			// cost
			// 
			this->cost->Text = L"cost (P/month)";
			this->cost->Width = 86;
			// 
			// rommListBox
			// 
			this->rommListBox->Controls->Add(this->roomList);
			this->rommListBox->Location = System::Drawing::Point(12, 112);
			this->rommListBox->Name = L"rommListBox";
			this->rommListBox->Size = System::Drawing::Size(510, 579);
			this->rommListBox->TabIndex = 2;
			this->rommListBox->TabStop = false;
			this->rommListBox->Text = L"Room list";
			// 
			// rentProcessBox
			// 
			this->rentProcessBox->Controls->Add(this->endDateTextField);
			this->rentProcessBox->Controls->Add(this->finalCostTextField);
			this->rentProcessBox->Controls->Add(this->finalCostLabel);
			this->rentProcessBox->Controls->Add(this->rentBtn);
			this->rentProcessBox->Controls->Add(this->endRentDateLabel);
			this->rentProcessBox->Controls->Add(this->rentDaysLabel);
			this->rentProcessBox->Controls->Add(this->rentDaysTextField);
			this->rentProcessBox->Controls->Add(this->startRentDateLabel);
			this->rentProcessBox->Controls->Add(this->startDateTextField);
			this->rentProcessBox->Controls->Add(this->fathLabel);
			this->rentProcessBox->Controls->Add(this->fathTextField);
			this->rentProcessBox->Controls->Add(this->lastNameLabel);
			this->rentProcessBox->Controls->Add(this->lastNameTextField);
			this->rentProcessBox->Controls->Add(this->firstNameLabel);
			this->rentProcessBox->Controls->Add(this->firstNameTextField);
			this->rentProcessBox->Location = System::Drawing::Point(543, 112);
			this->rentProcessBox->Name = L"rentProcessBox";
			this->rentProcessBox->Size = System::Drawing::Size(436, 579);
			this->rentProcessBox->TabIndex = 4;
			this->rentProcessBox->TabStop = false;
			this->rentProcessBox->Text = L"Rent process";
			// 
			// endDateTextField
			// 
			this->endDateTextField->Location = System::Drawing::Point(13, 358);
			this->endDateTextField->Multiline = false;
			this->endDateTextField->Name = L"endDateTextField";
			this->endDateTextField->ReadOnly = true;
			this->endDateTextField->Size = System::Drawing::Size(184, 25);
			this->endDateTextField->TabIndex = 20;
			this->endDateTextField->Text = L"";
			// 
			// finalCostTextField
			// 
			this->finalCostTextField->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->finalCostTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 21, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->finalCostTextField->ForeColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->finalCostTextField->Location = System::Drawing::Point(17, 446);
			this->finalCostTextField->Multiline = false;
			this->finalCostTextField->Name = L"finalCostTextField";
			this->finalCostTextField->ReadOnly = true;
			this->finalCostTextField->Size = System::Drawing::Size(383, 55);
			this->finalCostTextField->TabIndex = 19;
			this->finalCostTextField->Text = L"";
			// 
			// finalCostLabel
			// 
			this->finalCostLabel->AutoSize = true;
			this->finalCostLabel->Location = System::Drawing::Point(13, 408);
			this->finalCostLabel->Name = L"finalCostLabel";
			this->finalCostLabel->Size = System::Drawing::Size(76, 20);
			this->finalCostLabel->TabIndex = 18;
			this->finalCostLabel->Text = L"final cost:";
			// 
			// rentBtn
			// 
			this->rentBtn->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->rentBtn->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->rentBtn->Location = System::Drawing::Point(17, 511);
			this->rentBtn->Name = L"rentBtn";
			this->rentBtn->Size = System::Drawing::Size(383, 49);
			this->rentBtn->TabIndex = 6;
			this->rentBtn->Text = L"Rent room";
			this->rentBtn->UseVisualStyleBackColor = false;
			this->rentBtn->Click += gcnew System::EventHandler(this, &Form1::rentBtn_Click);
			// 
			// endRentDateLabel
			// 
			this->endRentDateLabel->AutoSize = true;
			this->endRentDateLabel->Location = System::Drawing::Point(13, 335);
			this->endRentDateLabel->Name = L"endRentDateLabel";
			this->endRentDateLabel->Size = System::Drawing::Size(108, 20);
			this->endRentDateLabel->TabIndex = 17;
			this->endRentDateLabel->Text = L"end rent date:";
			// 
			// rentDaysLabel
			// 
			this->rentDaysLabel->AutoSize = true;
			this->rentDaysLabel->Location = System::Drawing::Point(13, 268);
			this->rentDaysLabel->Name = L"rentDaysLabel";
			this->rentDaysLabel->Size = System::Drawing::Size(78, 20);
			this->rentDaysLabel->TabIndex = 15;
			this->rentDaysLabel->Text = L"rent days:";
			// 
			// rentDaysTextField
			// 
			this->rentDaysTextField->Location = System::Drawing::Point(13, 291);
			this->rentDaysTextField->Name = L"rentDaysTextField";
			this->rentDaysTextField->Size = System::Drawing::Size(184, 26);
			this->rentDaysTextField->TabIndex = 14;
			this->rentDaysTextField->TextChanged += gcnew System::EventHandler(this, &Form1::rentDaysTextField_TextChanged);
			// 
			// startRentDateLabel
			// 
			this->startRentDateLabel->AutoSize = true;
			this->startRentDateLabel->Location = System::Drawing::Point(9, 206);
			this->startRentDateLabel->Name = L"startRentDateLabel";
			this->startRentDateLabel->Size = System::Drawing::Size(113, 20);
			this->startRentDateLabel->TabIndex = 13;
			this->startRentDateLabel->Text = L"start rent date:";
			// 
			// startDateTextField
			// 
			this->startDateTextField->Location = System::Drawing::Point(13, 229);
			this->startDateTextField->Name = L"startDateTextField";
			this->startDateTextField->Size = System::Drawing::Size(184, 26);
			this->startDateTextField->TabIndex = 12;
			this->startDateTextField->TextChanged += gcnew System::EventHandler(this, &Form1::startDateTextField_TextChanged);
			// 
			// fathLabel
			// 
			this->fathLabel->AutoSize = true;
			this->fathLabel->Location = System::Drawing::Point(13, 148);
			this->fathLabel->Name = L"fathLabel";
			this->fathLabel->Size = System::Drawing::Size(41, 20);
			this->fathLabel->TabIndex = 11;
			this->fathLabel->Text = L"fath:";
			// 
			// fathTextField
			// 
			this->fathTextField->Location = System::Drawing::Point(99, 145);
			this->fathTextField->Name = L"fathTextField";
			this->fathTextField->Size = System::Drawing::Size(287, 26);
			this->fathTextField->TabIndex = 10;
			// 
			// lastNameLabel
			// 
			this->lastNameLabel->AutoSize = true;
			this->lastNameLabel->Location = System::Drawing::Point(13, 97);
			this->lastNameLabel->Name = L"lastNameLabel";
			this->lastNameLabel->Size = System::Drawing::Size(82, 20);
			this->lastNameLabel->TabIndex = 9;
			this->lastNameLabel->Text = L"last name:";
			// 
			// lastNameTextField
			// 
			this->lastNameTextField->Location = System::Drawing::Point(99, 94);
			this->lastNameTextField->Name = L"lastNameTextField";
			this->lastNameTextField->Size = System::Drawing::Size(287, 26);
			this->lastNameTextField->TabIndex = 8;
			// 
			// firstNameLabel
			// 
			this->firstNameLabel->AutoSize = true;
			this->firstNameLabel->Location = System::Drawing::Point(13, 43);
			this->firstNameLabel->Name = L"firstNameLabel";
			this->firstNameLabel->Size = System::Drawing::Size(83, 20);
			this->firstNameLabel->TabIndex = 7;
			this->firstNameLabel->Text = L"first name:";
			// 
			// firstNameTextField
			// 
			this->firstNameTextField->Location = System::Drawing::Point(99, 40);
			this->firstNameTextField->Name = L"firstNameTextField";
			this->firstNameTextField->Size = System::Drawing::Size(287, 26);
			this->firstNameTextField->TabIndex = 6;
			// 
			// searchBox
			// 
			this->searchBox->Controls->Add(this->minCostLabel);
			this->searchBox->Controls->Add(this->searchBtn);
			this->searchBox->Controls->Add(this->minCostTextField);
			this->searchBox->Controls->Add(this->minSquareLabel);
			this->searchBox->Controls->Add(this->minSqureTextField);
			this->searchBox->Controls->Add(this->adressLabel);
			this->searchBox->Controls->Add(this->adressTextField);
			this->searchBox->Location = System::Drawing::Point(12, 12);
			this->searchBox->Name = L"searchBox";
			this->searchBox->Size = System::Drawing::Size(967, 94);
			this->searchBox->TabIndex = 3;
			this->searchBox->TabStop = false;
			this->searchBox->Text = L"Search";
			// 
			// adressTextField
			// 
			this->adressTextField->Location = System::Drawing::Point(87, 42);
			this->adressTextField->Name = L"adressTextField";
			this->adressTextField->Size = System::Drawing::Size(254, 26);
			this->adressTextField->TabIndex = 0;
			// 
			// adressLabel
			// 
			this->adressLabel->AutoSize = true;
			this->adressLabel->Location = System::Drawing::Point(11, 44);
			this->adressLabel->Name = L"adressLabel";
			this->adressLabel->Size = System::Drawing::Size(70, 20);
			this->adressLabel->TabIndex = 1;
			this->adressLabel->Text = L"address:";
			// 
			// minSqureTextField
			// 
			this->minSqureTextField->Location = System::Drawing::Point(451, 41);
			this->minSqureTextField->Name = L"minSqureTextField";
			this->minSqureTextField->Size = System::Drawing::Size(112, 26);
			this->minSqureTextField->TabIndex = 2;
			// 
			// minSquareLabel
			// 
			this->minSquareLabel->AutoSize = true;
			this->minSquareLabel->Location = System::Drawing::Point(354, 45);
			this->minSquareLabel->Name = L"minSquareLabel";
			this->minSquareLabel->Size = System::Drawing::Size(91, 20);
			this->minSquareLabel->TabIndex = 3;
			this->minSquareLabel->Text = L"min square:";
			// 
			// minCostTextField
			// 
			this->minCostTextField->Location = System::Drawing::Point(650, 41);
			this->minCostTextField->Name = L"minCostTextField";
			this->minCostTextField->Size = System::Drawing::Size(112, 26);
			this->minCostTextField->TabIndex = 4;
			// 
			// searchBtn
			// 
			this->searchBtn->BackColor = System::Drawing::SystemColors::Info;
			this->searchBtn->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->searchBtn->Location = System::Drawing::Point(825, 30);
			this->searchBtn->Name = L"searchBtn";
			this->searchBtn->Size = System::Drawing::Size(122, 49);
			this->searchBtn->TabIndex = 1;
			this->searchBtn->Text = L"Search room";
			this->searchBtn->UseVisualStyleBackColor = false;
			this->searchBtn->Click += gcnew System::EventHandler(this, &Form1::searchBtn_Click);
			// 
			// minCostLabel
			// 
			this->minCostLabel->AutoSize = true;
			this->minCostLabel->Location = System::Drawing::Point(573, 44);
			this->minCostLabel->Name = L"minCostLabel";
			this->minCostLabel->Size = System::Drawing::Size(72, 20);
			this->minCostLabel->TabIndex = 5;
			this->minCostLabel->Text = L"min cost:";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(144, 144);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->ClientSize = System::Drawing::Size(991, 701);
			this->Controls->Add(this->rentProcessBox);
			this->Controls->Add(this->searchBox);
			this->Controls->Add(this->rommListBox);
			this->Name = L"Form1";
			this->Text = L"Room rent app";
			this->rommListBox->ResumeLayout(false);
			this->rentProcessBox->ResumeLayout(false);
			this->rentProcessBox->PerformLayout();
			this->searchBox->ResumeLayout(false);
			this->searchBox->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void searchBtn_Click(System::Object^ sender, System::EventArgs^ e)
	{
		string adress = ConvertUtills::textBoxStringToString(this->adressTextField->Text);
		string squareStr = ConvertUtills::textBoxStringToString(this->minSqureTextField->Text);
		string costStr = ConvertUtills::textBoxStringToString(this->minCostTextField->Text);
		if(ValidationUtills::isPositiveNumber(squareStr)
			&& ValidationUtills::isPositiveNumber(costStr)
			&& ValidationUtills::notEmptyString(adress))
		{
			int square = stoi(squareStr);
			int cost = stoi(costStr);
			userInterface->searchRoomByStreetAndMoneyAndSqure(false, adress, square, cost);
			updateRoomList();
		}
		else
		{
			string errors = "";
			if(!ValidationUtills::isPositiveNumber(squareStr))
			{
				errors += "min square must be positive number!\n\n";
			}
			if(!ValidationUtills::isPositiveNumber(costStr))
			{
				errors += "min cost must be positive number!\n\n";
			}
			if(!ValidationUtills::notEmptyString(adress))
			{
				errors += "adress is empty!\n\n";
			}
			displayErrorMessage(errors);
		}
	}
	private: System::Void rentBtn_Click(System::Object^ sender, System::EventArgs^ e)
	{
		string firstName = ConvertUtills::textBoxStringToString(this->firstNameTextField->Text);
		string lastName = ConvertUtills::textBoxStringToString(this->lastNameTextField->Text);
		string fath = ConvertUtills::textBoxStringToString(this->fathTextField->Text);
		string daysStr = ConvertUtills::textBoxStringToString(this->rentDaysTextField->Text);
		tm* startDate = ConvertUtills::textBoxStringToDate(this->startDateTextField->Text);
		if(ValidationUtills::notEmptyString(firstName)
			&& ValidationUtills::notEmptyString(lastName)
			&& ValidationUtills::notEmptyString(fath)
			&& ValidationUtills::isPositiveNumber(daysStr)
			&& ValidationUtills::dateIsCorrect(startDate)
			&& this->userInterface->selectedRoomIndex >= 0)
		{
			Renter* renter = new Renter(firstName, lastName, fath);
			int days = stoi(daysStr);
			userInterface->takeRoom(renter, 1000);
			updateRoomList();
		}
		else
		{
			string errors = "";
			if(!ValidationUtills::notEmptyString(firstName))
			{
				errors += "First name is empty!\n\n";
			}
			if(!ValidationUtills::notEmptyString(lastName))
			{
				errors += "Last name is empty!\n\n";
			}
			if(!ValidationUtills::notEmptyString(fath))
			{
				errors += "Fath name is empty!\n\n";
			}
			if(!ValidationUtills::isPositiveNumber(daysStr))
			{
				errors += "Rent days must be positive number!\n\n";
			}
			if(!ValidationUtills::dateIsCorrect(startDate))
			{
				errors += "Incorrect rent dates!\n\n";
			}
			if(this->userInterface->selectedRoomIndex < 0)
			{
				errors += "Room not selected!\n\n";
			}
			displayErrorMessage(errors);
		}
	}
	private: System::Void roomList_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
	{
		if(this->roomList->FocusedItem != nullptr && this->roomList->SelectedItems->Count > 0)
		{
			this->userInterface->selectedRoomIndex = this->roomList->FocusedItem->Index;
		}
		else if(this->roomList->SelectedItems->Count == 0)
		{
			this->userInterface->selectedRoomIndex = -1;
		}
		setFinalCost();
	}
	private: System::Void startDateTextField_TextChanged(System::Object^ sender, System::EventArgs^ e) { onDateChanged(); }
	private: System::Void rentDaysTextField_TextChanged(System::Object^ sender, System::EventArgs^ e) { onDateChanged(); }


	private: void setRentEndDate()
	{
		bool dateIsCorrect = ValidationUtills::dateIsCorrect(this->userInterface->startDate);
		int days = this->userInterface->rentDays;
		if(dateIsCorrect && days > 0 && days < 365)
		{
			this->endDateTextField->ForeColor = Color::Black;
			this->endDateTextField->Text = ConvertUtills::dateToTextBoxString(this->userInterface->endDate);
		}

	}
	private: void setFinalCost()
	{
		bool dateIsCorrect = ValidationUtills::dateIsCorrect(this->userInterface->startDate);
		int days = this->userInterface->rentDays;
		int selectedIndex = this->userInterface->selectedRoomIndex;
		if(dateIsCorrect && days > 0 && days < 365 && selectedIndex >= 0)
		{
			double cost = this->userInterface->getCurrentRoomCost();
			string costStr = to_string(cost);
			this->finalCostTextField->ForeColor = Color::Black;
			this->finalCostTextField->Text = ConvertUtills::stringToTextBoxString(costStr.substr(0, costStr.find_first_of('.')) + " P.");
		}
		else if(selectedIndex < 0)
		{
			printRedText(this->finalCostTextField, "select room!");
		}
		else
		{
			printRedText(this->finalCostTextField, "input error!");
		}
	}
	private: void onDateChanged()
	{
		int days = -1;
		string daysStr = ConvertUtills::textBoxStringToString(this->rentDaysTextField->Text);
		bool daysIsPosNumber = ValidationUtills::isPositiveNumber(daysStr);
		if(daysIsPosNumber)
		{
			days = stoi(daysStr);
		}
		string startDateStr = ConvertUtills::textBoxStringToString(this->startDateTextField->Text);
		bool isCorrectDate = ValidationUtills::dateIsCorrect(startDateStr);
		if(isCorrectDate)
		{
			tm* startDate = ConvertUtills::textBoxStringToDate(this->startDateTextField->Text);
			this->userInterface->startDate = startDate;
		}
		if(days > 0 && days < 365)
		{
			this->userInterface->rentDays = days;
		}
		if(days > 0 && days < 365 && isCorrectDate)
		{
			this->userInterface->updateRentEndDate();

			setRentEndDate();
		}
		else if(days < 0)
		{
			this->userInterface->rentDays = 0;
			printRedText(this->endDateTextField, "wrong days count!");
		}
		else if(days > 365)
		{
			this->userInterface->rentDays = 0;
			printRedText(this->endDateTextField, "too mach days!");
		}
		else if(this->userInterface->startDate != NULL && !daysIsPosNumber)
		{
			this->userInterface->rentDays = 0;
			setFinalCost();
		}
		else if(!isCorrectDate)
		{
			this->userInterface->rentDays = 0;
			printRedText(this->endDateTextField, "wrong date!");
		}
		else
		{
			this->userInterface->rentDays = 0;
			printRedText(this->endDateTextField, "wrong inputs!");
		}
		setFinalCost();
	}
	private: void printRedText(RichTextBox^ textBox, string str)
	{
		textBox->ReadOnly = false;
		textBox->ForeColor = Color::Red;
		textBox->ReadOnly = true;
		textBox->Text = ConvertUtills::stringToTextBoxString(str);
	}
	private: void displayErrorMessage(string error)
	{
		System::Windows::Forms::DialogResult result = MessageBox::Show(ConvertUtills::stringToTextBoxString(error), "Wrong input!", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	private: void updateRoomList()
	{
		for(Room room : userInterface->currentRoomList)
		{
			String^ id = ConvertUtills::stringToTextBoxString(to_string(room.id));
			String^ adress = ConvertUtills::stringToTextBoxString(room.address);
			String^ square = ConvertUtills::stringToTextBoxString(to_string(room.square));
			String^ cost = ConvertUtills::stringToTextBoxString(to_string(room.moneyPerMonth));
			ListViewItem^ roomItem = gcnew ListViewItem();
			roomItem->Text = id;
			roomItem->SubItems->Add(adress);
			roomItem->SubItems->Add(square);
			roomItem->SubItems->Add(cost);
			this->roomList->Items->Add(roomItem);
		}
	}
	};
}
