#pragma once
#include "Room.h"
class RentController
{

public:

	/**
	* Make rent deal
	*
	* @param money Money provided by renter
	* @param room Room to rent
	*
	* @return Is deal completed successfuly
	**/
	bool payForRent(int money, Room room);

private:

	double currentMoney = 0;


};

