#pragma once
#include <string>
#include <windows.h>
#include <iostream>
#include "Renter.h"
#include <ctime>
#include <time.h>
using namespace std;

class Room
{

public:

	int id;
	string address = "";
	int houseNumber = -1;
	int moneyPerMonth = -1;
	Renter* renter = NULL;
	int square = -1;
	bool isRented = false;
	tm rentStartDate;
	tm rentEndDate;
	int rentedDays = -1;

	Room(string address, int houseNumber, int square, int moneyPerMonth)
	{
		this->address = address;
		this->houseNumber = houseNumber;
		this->square = square;
		this->moneyPerMonth = moneyPerMonth;
		this->id = Room::curentId;
		Room::curentId++;
	}

	/**
	* Turn room to rented state
	*
	* @param renter New room renter
	* @param rentedDays Count of rent days
	**/
	void startRent(Renter* renter, int rentedDays);

	/**
	* Turn room to not rented state
	**/
	void endRent();

	/**
	* Save this Romm to file
	*
	* @param ofStream File stream to save this Room
	**/
	void serealize(ostream& ofStream);

	/**
	* Load next Romm from file
	*
	* @param ofStream File stream to load next Room
	*
	* @return Room instance - Loaded room
	**/
	static Room* deserealize(istream& ofStream);

private:

	static int curentId;

	Room() {}

};

