#pragma once
#include <string>
#include <regex>
using namespace System;
using namespace std;

class ConvertUtills
{

public: 

	static string textBoxStringToString(String^ stringToConvert);
	static String^ stringToTextBoxString(string str);
	static String^ dateToTextBoxString(tm* date);
	static tm* textBoxStringToDate(String^ stringToConvert);
	static tm* stringToDate(string stringToConvert);
	
};

