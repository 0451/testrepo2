#pragma once
#include <string>
#include <iostream>
using namespace std;

class Renter
{

public:

	string firstName;
	string lastName;
	string fath;

	Renter() {}

	Renter(string firstName, string lastName, string fath)
	{
		this->firstName = firstName;
		this->lastName = lastName;
		this->fath = fath;
	}

	/**
	* Save this Renter to file
	*
	* @param ofStream File stream to save this Renter
	**/
	void serealize(ostream& ofStream);

	/**
	* Load next Renter from file
	*
	* @param ofStream File stream to load next Renter
	*
	* @return Renter instance - Loaded renter
	**/
	static Renter* deserealize(istream& ofStream);

};

