#include "UserInterface.h"

double UserInterface::getCurrentRoomCost()
{
	return this->currentRoomList [this->selectedRoomIndex].moneyPerMonth / 30 * this->rentDays;
}

bool UserInterface::searchRoomByStreetAndMoneyAndSqure(bool rented, string address, int square, int money)
{
	return false;
}

bool UserInterface::getAllRooms()
{
	Room room("adress1", 1, 100, 20000);
	Room room1("adress2", 2, 200, 30000);
	Room room2("adress3", 3, 300, 40000);
	this->currentRoomList.push_back(room);
	this->currentRoomList.push_back(room1);
	this->currentRoomList.push_back(room2);
	return true;
}

bool UserInterface::takeRoom(Renter* renter, int sumToPay)
{
	return false;
}

void UserInterface::updateRentEndDate()
{
	tm* endDate = this->startDate;
	time_t timeNow = time(0);
	endDate->tm_mday += this->rentDays;
	timeNow = mktime(endDate);
	localtime_s(endDate, &timeNow);
	this->endDate = endDate;
}

