#include "Renter.h"
#include <iostream>
using namespace std;

void Renter::serealize(ostream& ofStream)
{
	int firstNameSize = (this->firstName.size());
	int lastNameSize = (this->lastName.size());
	int fathSize = (this->fath.size());

	ofStream.write(reinterpret_cast<char*>(&firstNameSize), sizeof(int));
	ofStream.write(this->firstName.c_str(), firstNameSize);

	ofStream.write(reinterpret_cast<char*>(&lastNameSize), sizeof(int));
	ofStream.write(this->lastName.c_str(), lastNameSize);

	ofStream.write(reinterpret_cast<char*>(&fathSize), sizeof(int));
	ofStream.write(this->fath.c_str(), fathSize);
}

Renter* Renter::deserealize(istream& inStream)
{
	Renter* renter = new Renter();

	int firstNameSize;
	int lastNameSize;
	int fathSize;

	char* buf;

	inStream.read(reinterpret_cast<char*>(&firstNameSize), sizeof(int));
	buf = new char [firstNameSize];
	inStream.read(buf, firstNameSize);
	renter->firstName = "";
	renter->firstName.append(buf, firstNameSize);

	inStream.read(reinterpret_cast<char*>(&lastNameSize), sizeof(int));
	buf = new char [lastNameSize];
	inStream.read(buf, lastNameSize);
	renter->lastName = "";
	renter->lastName.append(buf, lastNameSize);

	inStream.read(reinterpret_cast<char*>(&fathSize), sizeof(int));
	buf = new char [fathSize];
	inStream.read(buf, fathSize);
	renter->fath = "";
	renter->fath.append(buf, fathSize);
	return renter;
}
